package ex02;

public interface User {
	public void playMusic();
	public void playVideo() throws Exception;
	public void playVideoHD() throws Exception;
}
