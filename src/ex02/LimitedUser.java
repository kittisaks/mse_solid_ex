package ex02;

public class LimitedUser implements User {

	public LimitedUser() {
		
	}

	@Override
	public void playMusic() {
		System.out.println("01 Music Played");
		
	}

	@Override
	public void playVideo() throws Exception {
		throw new Exception();
		
	}

	@Override
	public void playVideoHD() throws Exception {
		throw new Exception();
	}
	

}
