package ex02;

public class Ex02_Main {
	public static void main(String[] args) {
		
		LimitedUser  lu = new LimitedUser();
		StandardUser su = new StandardUser();
		PremiumUser  pu = new PremiumUser();
		
		try {
			lu.playMusic();
			lu.playVideo();
			lu.playVideoHD();
		} catch (Exception e) {
			System.out.println("Cannot perform further operations");
		}
		
		try {
			su.playMusic();
			su.playVideo();
			su.playVideoHD();
		} catch (Exception e) {
			System.out.println("Cannot perform further operations");
		}
		
		try {
			pu.playMusic();
			pu.playVideo();
			pu.playVideoHD();
		} catch (Exception e) {
			System.out.println("Cannot perform further operations");
		}
	}
}
