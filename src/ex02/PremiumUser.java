package ex02;

public class PremiumUser implements User{

	@Override
	public void playMusic() {
		System.out.println("01 Music Played");
	}

	@Override
	public void playVideo() throws Exception {
		System.out.println("02 Video played");
		
	}

	@Override
	public void playVideoHD() throws Exception {
		System.out.println("03 Video HD played");
	}

}
