package ex05;

public class Ex05_Main {
	public static void main(String[] args){
		
		Vehicle car = new Vehicle(VehicleType.Car);
		Vehicle truck = new Vehicle(VehicleType.Truck);
		Vehicle plane = new Vehicle(VehicleType.Plane);
		
		try {
			plane.run();
			plane.load();
			plane.fly();
			
			truck.run();
			truck.load();
			truck.fly();
			
			car.run();
			car.load();
			car.fly();
		}
		catch (Exception e) {
			System.out.println("Program contains bug!");
		}
	}
}
