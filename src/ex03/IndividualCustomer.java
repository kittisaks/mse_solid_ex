package ex03;

public class IndividualCustomer implements IBankCustomer {

	private String name;
	private String telephone;
	private Integer salary;
	private String orgName;
	
	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String geTelephone() {
		return telephone;
	}

	@Override
	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	@Override
	public Integer getSalary() {
		return salary;
	}

	@Override
	public void setOrgName(String org) {
		this.orgName = org;
	}

	@Override
	public String getOrgName() {
		return orgName;
	}

	@Override
	public void setCeoName(String ceoName) throws Exception {
		throw new Exception();
		
	}

	@Override
	public String getCeoName() throws Exception {
		throw new Exception();
	}

	@Override
	public void setRegisteredCapital(Integer regCap) throws Exception {
		throw new Exception();
		
	}

	@Override
	public Integer getRegisteredCapital() throws Exception {
		throw new Exception();
	}

}
