package ex03;

public class EnterpriseCustomer implements IBankCustomer {

	private String name;
	private String telephone;
	private String orgName;
	private String ceoName;
	private Integer regCap;
	
	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String geTelephone() {
		return telephone; 
	}

	@Override
	public void setSalary(Integer salary) throws Exception {
		throw new Exception();
		
	}

	@Override
	public Integer getSalary() throws Exception {
		throw new Exception();
	}

	@Override
	public void setOrgName(String org) {
		this.orgName = org;
	}

	@Override
	public String getOrgName() {
		return orgName;
	}

	@Override
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}

	@Override
	public String getCeoName() {
		return ceoName;
	}

	@Override
	public void setRegisteredCapital(Integer regCap) {
		this.regCap = regCap;
	}

	@Override
	public Integer getRegisteredCapital() {
		return regCap;
	}

}
