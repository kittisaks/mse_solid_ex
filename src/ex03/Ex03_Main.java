package ex03;

import java.util.ArrayList;
import java.util.List;

public class Ex03_Main {
	public static void main(String[] args){
		
		//Individual Bank Customer
		IndividualCustomer ic = new IndividualCustomer();
		try {
			ic.setName("Kitty");
			ic.setTelephone("123456789");
			ic.setSalary(1000);
			ic.setOrgName("My Kitty Co");
			ic.setRegisteredCapital(10000000);
			ic.setCeoName("Invalid Kitty");
		} 
		catch (Exception e) {
			System.out.println("Cannot Perform operation");
		}
		
		
		//Enterprise Bank Customer
		EnterpriseCustomer ec = new EnterpriseCustomer();
		try {
			ec.setName("Some New Company");
			ec.setTelephone("987654321");
			ec.setSalary(1000);
			ec.setOrgName("Some New Company Inc");
			ec.setRegisteredCapital(10000000);
			ec.setCeoName("Kitty");
		} 
		catch (Exception e) {
			System.out.println("Cannot perform operation");
		}
		
		List<IBankCustomer> custList = new ArrayList<IBankCustomer>();
		custList.add(ic);
		custList.add(ec);
		
		for (IBankCustomer cust : custList) {
			try {
				System.out.println("=== " + cust.getName() + " ===");
				System.out.println("\tOrg name  : " + cust.getOrgName());
				System.out.println("\tCEO name  : " + cust.getCeoName());
				System.out.println("\tTelephone : " + cust.geTelephone());
				System.out.println("\tReg. Cap. : " + cust.getRegisteredCapital());
				System.out.println("\tSalary    : " + cust.getSalary());
			} 
			catch (Exception e) {
				System.out.println("Cannot perform oepration");
			}
		}
	}
}
