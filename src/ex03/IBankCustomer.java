package ex03;

public interface IBankCustomer {
	public void setName(String name);
	public String getName();
	
	public void setTelephone(String telephone);
	public String geTelephone();
	
	public void setSalary(Integer salary) throws Exception;
	public Integer getSalary() throws Exception;
	
	public void setOrgName(String org);
	public String getOrgName();
	
	public void setCeoName(String ceoName) throws Exception;
	public String getCeoName() throws Exception;
	
	public void setRegisteredCapital(Integer regCap) throws Exception;
	public Integer getRegisteredCapital() throws Exception;
}
