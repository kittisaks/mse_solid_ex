package ex04;

public class CustomerList {

	private int dbType;
	
	public CustomerList(int dbType) {
		this.dbType = dbType;
	}
	
	public void ImportToDb() {
		switch (dbType) {
		case 0:
			fileDb fdb = new fileDb("CustListDb.db");
			fdb.open();
			fdb.write(this);
			fdb.close();
			break;
		case 1:
			keyValueDb kvdb = new keyValueDb("host", 8080);
			kvdb.connect();
			kvdb.write(this);
			kvdb.close();
			break;
		case 2:
			noSqlDb nsdb = new noSqlDb("host", 8080);
			nsdb.connect();
			nsdb.write(this);
			nsdb.close();
			break;
		case 3:
			sqlDb sdb = new sqlDb("host", 8080);
			sdb.connect();
			sdb.write(this);
			sdb.close();
			break;
		}
	}
	
}
