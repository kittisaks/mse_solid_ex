package ex01;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class User {
	
	private String name;
	private String address;
	private String phone;
	private final String fileDb = "userdb.txt";
	
	public User(String name, String address, String phone) {
		this.name = name;
		this.address = address;
		this.phone = phone;
	}
	
	public void Register() {
		
		if (!isFileDbExist(fileDb))
			createFileDb(fileDb);
		
		addUserToFileDb(fileDb, name, address, phone);
	}
	
	private boolean isFileDbExist(String fileDb) {
		File db = new File(fileDb);
		return db.exists();
	}
	
	private File createFileDb(String fileDb) {
		File db = new File(fileDb);
		try {
			if (db.createNewFile())
				return db;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void addUserToFileDb(String fileDb, String name, String address, String phone) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileDb, true));
			writer.newLine();
			writer.write(name + ", " + address + ", " + phone);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
